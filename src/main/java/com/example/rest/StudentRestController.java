package com.example.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RestController
@RequestMapping("/rest")
public class StudentRestController {
	
	@Autowired
    StudentService studentDAO;
	
	@RequestMapping("/student/view/{npm}")
	public StudentModel view(@PathVariable("npm") String npm){
		return studentDAO.selectStudent(npm);
	}
	
	@RequestMapping("/student/viewall")
    public List<StudentModel> view ()
    {
        return studentDAO.selectAllStudents ();
    }

}
