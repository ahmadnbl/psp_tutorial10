package com.example.controller;

import java.util.function.LongToIntFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.CourseModel;
import com.example.service.CourseService;

import lombok.extern.log4j.Log4j;


@Log4j
@Controller
public class CourseController {
	
	@Autowired
    CourseService courseDAO;
	
	@RequestMapping("/course/view/{idCourse}")
    public String viewPath (Model model, @PathVariable(value = "idCourse") String idCourse)
    {
		CourseModel course = courseDAO.selectCourse(idCourse);
        model.addAttribute ("course", course);
        log.info ("students= " + course.getStudents().size());
        return "course-view";
    }

}
