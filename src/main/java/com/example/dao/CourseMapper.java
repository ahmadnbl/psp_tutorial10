package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.model.CourseModel;
import com.example.model.StudentModel;

@Mapper
public interface CourseMapper {

	@Select("select id_course, nama, sks from course where id_course = #{id_course}")
    @Results(value = {
			@Result(property="idCourse", column="id_course"), 
			@Result(property="nama", column="nama"), 
			@Result(property="sks", column="sks"),
			@Result(property="students", column="id_course", javaType = List.class, many=@Many(select="selectStudents"))
			})
	CourseModel selectCourse (@Param("id_course") String idCourse);
	
	@Select("select s.npm, s.name, s.gpa from student s, studentcourse sc where s.npm = sc.npm and sc.id_course = #{idCourse}")
    List<StudentModel> selectStudents (@Param("idCourse") String idCourse);
	
	@Select("select course.id_course, nama, sks " +
            "from studentcourse join course " +
            "on studentcourse.id_course = course.id_course ")
    @Results(value={
    		@Result(property="idCourse", column="id_course"),
    		@Result(property="nama", column="nama"),
    		@Result(property="sks", column="sks"),
    		@Result(property="students", column="id_course", javaType = List.class, many=@Many(select="selectStudents"))
    })
    List<CourseModel> selectAllCourses ();
	
}
